<?php

use Illuminate\Database\Seeder;
use App\Models\Bears\Bear;

class BearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Bear::class,120)->create();
    }
}
