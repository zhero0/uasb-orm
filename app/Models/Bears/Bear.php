<?php

namespace App\Models\Bears;

use Illuminate\Database\Eloquent\Model;

class Bear extends Model
{
    protected $table = "bears";

    protected $fillable = [
        'name',
        "danger_level"
    ];

    protected $casts =  [
        "danger_level"  => "integer",
        'updated_at'    => "date",
        'created_at'    => "date"
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "id"
    ];

    protected $appends = [
        "is_bad"
    ];


    public function trees()
    {
        return $this->belongsToMany(Tree::class);
    }


    public function getIsBadAttribute()
    {
        return (boolean) ($this->danger_level >= 3);
    }

}
