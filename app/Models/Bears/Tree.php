<?php

namespace App\Models\Bears;

use Illuminate\Database\Eloquent\Model;

class Tree extends Model
{
    protected $table = "trees";

    protected $fillable = [
        'type',
        "age"
    ];

    protected $casts =  [
        "age"  => "integer",
        'updated_at'    => "date",
        'created_at'    => "date"
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "id"
    ];

}
