<?php

use App\Models\Bears\Bear;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// pruebas de queries a la tabla de osos

/**
 * Seleccionar todos los osos
 */
Route::get('bears', function () {
    return Bear::get();
});

/**
 * insertar un nuevo registro de oso ro
 */
Route::get('bears/create', function () {

    $new_bear = Bear::create([
         'name'         => "ro",
         'danger_level' => 5,
    ]);

    return $new_bear;
});

/**
 * actualizar el nombre de un oso llamado ro
 */
Route::get('bears/{bear_id}', function ($bear_id) {
    return Bear::whereHas("trees",function($q){
        return $q->where("type" ,"=","robles");
    })->with("trees")->find($bear_id);
});

/**
 * selecionar los osos por grado de peligro
 */
Route::get('bears/danger_levels/{level}', function ($level) {
    return Bear::where("danger_level", "=",$level)->get();
});
